﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KosarajuAlgorithm
{
    class Program
    {
        enum Colors { White, Gray, Black};

        static void DFS(int vertex, int[,] adjacencyMatrix, Colors[] colorArray, int[] orderArray, ref int currentPosition)
        {
            int lenght = adjacencyMatrix.GetLength(0);

            colorArray[vertex] = Colors.Gray;
            for(int i = 0; i < lenght; i++)
            {
                if(adjacencyMatrix[vertex,i] != 0 && colorArray[i] == Colors.White)
                {
                    DFS(i, adjacencyMatrix, colorArray, orderArray, ref currentPosition);
                }
            }
            orderArray[currentPosition] = vertex;
            currentPosition++;
            colorArray[vertex] = Colors.Black;
        }

        static void DFS_Invert(int vertex, int[,] adjacencyMatrix, Colors[] colorArray, int[] componentID, int componentCount)
        {
            int lenght = adjacencyMatrix.GetLength(0);

            colorArray[vertex] = Colors.Gray;
            for (int i = 0; i < lenght; i++)
            {
                if (adjacencyMatrix[i, vertex] != 0 && colorArray[i] == Colors.White)
                {
                    DFS_Invert(i, adjacencyMatrix, colorArray, componentID, componentCount);
                }
            }
            componentID[vertex] = componentCount;
            colorArray[vertex] = Colors.Black;
        }

        static int[,] Kosaraju(int[,] adjacencyMatrix)
        {
            int lenght = adjacencyMatrix.GetLength(0);
            int currentPosition = 0;
            int componentCount = 0;
            int[] orderArray = new int[lenght];
            int[] componentID = new int[lenght];
            Colors[] colorArray = new Colors[lenght];
            for(int i = 0; i < lenght; i++)
            {
                if(colorArray[i] == Colors.White)
                {
                    DFS(i, adjacencyMatrix, colorArray, orderArray, ref currentPosition);
                }
            }
            for(int i = 0; i < lenght; i++)
            {
                colorArray[i] = Colors.White;
            }
            for (int i = lenght - 1; i >= 0; i--)
            {
                if (colorArray[orderArray[i]] == Colors.White)
                {
                    DFS_Invert(orderArray[i], adjacencyMatrix, colorArray, componentID, componentCount);
                    componentCount++;
                }
            }

            int[,] componentAdjacencyMatrix = new int[componentCount, componentCount];
            for(int i = 0; i < lenght; i++)
            {
                for(int j = 0; j < lenght; j++)
                {
                    if(adjacencyMatrix[i,j] != 0 && componentID[i] != componentID[j])
                    {
                        componentAdjacencyMatrix[componentID[i], componentID[j]] = 1;
                    }
                }
            }
            return componentAdjacencyMatrix;
        }
        static void Main(string[] args)
        {
            
            System.Console.WriteLine("Введите порядок матрицы смежности: ");
            int lenght = Convert.ToInt32(System.Console.ReadLine());
            int[,] adjacencyMatrix = new int[lenght, lenght];
            System.Console.WriteLine("Введите элементы матрицы смежности построчно: ");
            for (int i = 0; i < lenght; i++)
            {
                string buffer = System.Console.ReadLine();
                string[] numbers = buffer.Split(' ');
                for(int j = 0; j < lenght; j++)
                {
                    adjacencyMatrix[i,j] = Convert.ToInt32(numbers[j]);
                }
            }
            System.Console.WriteLine();

            //int[,] adjacencyMatrix = new int[8, 8];
            //matrix[0, 1] = 1;
            //matrix[1, 2] = 1;
            //matrix[1, 4] = 1;
            //matrix[1, 5] = 1;
            //matrix[2, 3] = 1;
            //matrix[2, 6] = 1;
            //matrix[3, 2] = 1;
            //matrix[3, 7] = 1;
            //matrix[4, 0] = 1;
            //matrix[4, 5] = 1;
            //matrix[5, 6] = 1;
            //matrix[6, 5] = 1;
            //matrix[7, 3] = 1;
            //matrix[7, 6] = 1;

            int[,] res = Kosaraju(adjacencyMatrix);

            for(int i = 0; i < res.GetLength(0); i++)
            {
                for (int j = 0; j < res.GetLength(0); j++)
                {
                    System.Console.Write("{0} ", res[i, j]);
                }
                System.Console.WriteLine();
            }
            System.Console.ReadLine();
            //string a = System.Console.ReadLine();
            //System.Console.WriteLine(a);
        }
    }
}
